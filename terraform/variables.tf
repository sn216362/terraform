variable "access_key" {
    type = string
    # default = "Undefined environment variable $AWS_ACCESS_KEY_ID"
    default = "$TF_VAR_access_key"
}

variable "secret_key" {
    type = string
    default = "$TF_VAR_secret_key"
}

variable "region" {
    type = string
    default = "$TF_VAR_region"
}

variable "instance_type" {
    type = string
    default = "$TF_VAR_instance_type"
}

variable "ubuntu" {
    type = string
    default = "$TF_VAR_ami_ubuntu"
}
