provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region = var.region
}

resource "aws_instance" "ec2_ubuntu" {
    ami = var.ubuntu
    instance_type = var.instance_type
    associate_public_ip_address = true
}

output "aws_instance_ec2_ubuntu" {
    value = aws_instance.ec2_ubuntu
}
